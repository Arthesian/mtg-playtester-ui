const PlayerStore = {
    namespaced: true,
    state: () => ({ 
        lifeCounter: { name: "Life", amount: 40 },
        counters: []
     }),
     actions: { 

    },
    mutations: { 
        addCounter(state, { name }) {
            state.counters.push({ name, amount: 0 })
        },
        removeCounter(state, counter) {
            let index = state.counters.indexOf(counter);
            if(index > -1) {
                state.counters.splice(index, 1);
            }
        }
     },
  
    getters: { 
        life(state) {
            return state.life;
        }
     }
  }

  export default PlayerStore;