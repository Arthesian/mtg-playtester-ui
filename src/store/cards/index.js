import Zones from "../../enums/Zones";

import axios from 'axios'
import Card from "../../models/Card";
import helper from "../../helpers/helper";
import CardsFromLibraryModes from "../../enums/CardsFromLibraryModes";

const CardsStore = {
    namespaced: true,
    state: () => ({
        deck: null,
        cardSleeveUrl: '',
        origData: [],
        cards: [],
        commanders: [],
        cardsFromLibraryMode: CardsFromLibraryModes.Reveal,
        tokens: [],
        boardSize: "normal"
    }),
    actions: {
        async getDeckFromApi({ commit, state }, deck) {

            commit('setLoading', true, { root: true });

            try {

                commit('clearData');
                commit('setDeck', deck);
    
                let response = await axios.get(`https://mtg-playtester.herokuapp.com/parseDeckUrl?url=${deck.url}`);
    
                if (response) {
    
                    setTimeout(() => {
                        
                        commit('setOriginalCardData', response.data.cards);
                        commit('setCards');
                        commit('setCommanders', response.data.commanders);
        
                        // Move Commanders to CommandZone
                        setTimeout(async () => {
                            if(state.commanders.length) {
                                let commanderCards = state.cards.filter(c => state.commanders.includes(c.data.card.name));
                                if (commanderCards.length) {
                                    for (let commander of commanderCards) {
                                        let cardHtml = await helper.getCardHtmlFromCard(commander);
                                        helper.moveCardHtmlToZone(cardHtml, Zones.CommandZone, this);
                                    }
                                }
                            }
                        }, 0);
    
                    }, 0);
                }

            } catch(err) {
                console.error(err);
                if(err && err.message) {
                    alert('Something went wrong while loading the deck:\n\n[ERROR]: ' + err.message + '\n\nPlease double check your URL');
                } else {
                    alert('Something went wrong while loading the deck:\n\n[ERROR]: ' + JSON.stringify(err));
                }
            } finally {
                commit('setLoading', false, { root: true });
            }
        },

        resetGame({ dispatch, commit, state }) {
            dispatch('getDeckFromApi', state.deck);
            commit('removeAllTokens');
        },

        moveCard({ commit }, { card, zone }) {

            // get card original html;
            let cardElement = document.querySelector(`.card-wrapper[card-index="${card.index}"]`);
            let targetZoneElement = document.querySelector(`.card-container[type="${zone}"]`);

            if(!cardElement) { console.log(`Could not find Card HTML element for:`, card); }
            if(!targetZoneElement) { console.log(`Could not find the Target zone HTML Element for:`, zone); }

            targetZoneElement.append(cardElement);

            commit('moveCard', { card, zone });
            return card;
        },

        getCardByIndex({ state }, index) {
            return state.cards.find(c => c.index == index);
        },

        createToken({ commit }, tokenData) {

            let cardData = { card: tokenData };

            let card = new Card(cardData);
            card.index = new Date().getTime(); 
            card.isInZone = Zones.Playfield;
            card.isVisible = true;

            commit('createToken', card);
        },

        removeToken({ commit, state }, tokenHtml) {
            let tokenIndex = tokenHtml.getAttribute('card-index');
            let token = state.tokens.find(t => t.index == tokenIndex);

            if(token) {
                commit('removeToken', token);
            }
        },

        untapAll({ getters }) {
            getters.playfield.forEach(c => { c.isTapped = false; })
        }
    },
    mutations: {
        setCardVisibility(state, { card, visible }) {
            card.isVisible = visible;
        },
        moveCard(state, { card, zone }) {
            let validZones = Object.values(Zones);
            if(!validZones.includes(zone)) { console.error('Card was attempted to move to an invalid zone:', card, zone); return; }

            // Set zone of card
            card.isInZone = zone;

            if(card.isInZone !== Zones.Playfield) {
                card.isVisible = true;
            }

            if(card.isInZone == Zones.Deck) {
                card.isVisible = false;
            }
        
            if(card.isInZone != Zones.Playfield) {
                card.isTapped = false;
            }
        },
        
        setOriginalCardData(state, cardsJson) {

            // Split duplicate cards to unique cards (8x basic lands etc...)
            let unDuplicated = [];
            cardsJson.forEach(c => {
                for (let x = 0; x < c.amount; x++) {
                    unDuplicated.push(c);
                }
            })

            // Deep clone copy of original set of card data
            state.origData = unDuplicated; // Clone for Reset;
        },

        setCommanders(state, names) {
            state.commanders = names;
        },

        clearData(state) {
            state.cards = [];
            state.setOriginalCardData = [];
            state.commanders = [];
            state.tokens = [];
        },

        setCards(state) {
            let clone = state.origData.map(c => Object.assign({}, c));

            state.cards = clone.map((c, i) => {
                let card = new Card(c);
                card.index = i; 
                card.isInZone = Zones.Deck;
                card.isVisible = false;
                card.creates = c.creates;
                return card;
            });

            helper.shuffle(state.cards);
        },

        cardsFromLibraryMode(state, mode) {

            let validModes = Object.values(CardsFromLibraryModes);
            if(!validModes.includes(mode)) { console.log(`Invalid deck preview mode was selected! "${mode}"`); return; }

            state.cardsFromLibraryMode = mode;
        },

        createToken(state, data) {
            state.tokens.push(data);
        },

        removeToken(state, data) {
            let tokenIndex = state.tokens.indexOf(data);
            state.tokens.splice(tokenIndex, 1);
        },

        removeAllTokens(state) {
            state.tokens = [];
        },

        setDeck(state, deck) {
            state.deck = deck;
        },

        setCardSleeveUrl(state, url) {
            state.cardSleeveUrl = url;
        },

        setBoardSize(state, size) {
            state.boardSize = size;
        }
    },

    getters: {
        currentLoadedDeck(state) {
            return state.deck;
        },
        totalNumberOfCards(state) {
            return state.origData.length;
        },
        all(state) {
            return state.cards;
        },
        deck(state) {
            return state.cards.filter(c => c.isInZone == Zones.Deck);
        },
        hand(state) {
            return state.cards.filter(c => c.isInZone == Zones.Hand);
        },
        playfield(state) {
            return [...state.cards.filter(c => c.isInZone == Zones.Playfield), ...state.tokens];
        },
        commandzone(state) {
            return state.cards.filter(c => c.isInZone == Zones.CommandZone);
        },
        graveyard(state) {
            return state.cards.filter(c => c.isInZone == Zones.Graveyard);
        },
        exile(state) {
            return state.cards.filter(c => c.isInZone == Zones.Exile);
        },
        cardsFromLibrary(state) {
            return state.cards.filter(c => c.isInZone == Zones.DeckPreview);
        },
        createsToken(state) {
            return state.cards.filter(c => c.data.creates && c.data.creates.length);
        },
        tokens(state) {
            return state.tokens;
        },
        previewMode(state) {
            return state.cardsFromLibraryMode;
        },
        sleeveImageUrl(state) {
            return state.cardSleeveUrl || require("@/assets/cardback.webp"); 
        }
    }
}

export default CardsStore;