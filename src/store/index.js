import Vue from 'vue'
import Vuex from 'vuex'

import Player from "./player";
import Cards from "./cards";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        cards: Cards,
        player: Player,
    },
    state: () => ({ 
        loading: false,
        helpOpen: false,
        playmatImageUrl: '',
    }),
    actions: {

    },
    mutations: {
        setLoading(state, boolean) {
            state.loading = boolean;
        },
        setPlaymatUrl(state, url) {
            state.playmatImageUrl = url;
        }
    },
    getters: {
        isLoading(state) {
            return state.loading;
        },
        playmatImageUrl(state) {
            return state.playmatImageUrl || require("@/assets/blacklotus.jpg");
        }
    }
  })

  export default store;