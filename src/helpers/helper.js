import Zones from './../enums/Zones';

class Helper {
    shuffle(list, isHtml) {
        if (isHtml) {
            for (var i = list.children.length; i >= 0; i--) {
                list.appendChild(list.children[Math.random() * i | 0]);
            }
        } else {
            for (let i = list.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * i)
                const temp = list[i]
                list[i] = list[j]
                list[j] = temp
            }
        }
    }

    async moveXCardsFromTopOfDeckToZone(numberOfCards, zone, store) {
        for (let i = 0; i < numberOfCards; i++) {
            let topCard = await this.getTopCardFromDeck();
            await this.moveCardHtmlToZone(topCard, zone, store);
        }
    }

    async getTopCardFromDeck() {
        let deck = document.querySelector('.deck.card-stack');
        let topCard = deck.children[deck.children.length - 1];
        return topCard;
    }

    async getCardHtmlFromCard(card) {
        return document.querySelector(`.card-container .card-wrapper[card-index="${card.index}"]`);
    }

    async moveCardHtmlToZone(cardElement, zone, store, onBottom) {

        let validZones = Object.values(Zones);
        let targetZone = zone;

        if (typeof zone != "string") {
            zone = targetZone.getAttribute("type");
        }

        // Exceptions for TOP/BOT
        if (zone == Zones.DeckTop) {
            zone = Zones.Deck;
            onBottom = false;
            targetZone = document.querySelector(`.card-container[type="${zone}"]`);
        }
        if (zone == Zones.DeckBot) {
            zone = Zones.Deck;
            onBottom = true;
            targetZone = document.querySelector(`.card-container[type="${zone}"]`);
        }

        // Either get HTML or TargetZone Name
        if (typeof targetZone == "string") {
            targetZone = document.querySelector(`.card-container[type="${zone}"]:not(.extra-slot)`);
            if (!targetZone) { console.error('Could not find target zone HTML element for: ' + zone); return; }
        }

        if (!validZones.includes(zone)) { console.error('Card moved to invalid zone: ' + zone); return; }

        if (cardElement.attributes.token) {
            this.handleTokenMove(cardElement, targetZone, zone, store); return;
        }

        let card = await this.getCardFromSourceByElement(cardElement, store);
        if (!card) { console.error('Could not find card for: ', cardElement); return; }

        // Check if the card must become face-down
        if ([Zones.Deck, Zones.Hand].includes(zone)) { card.isVisible = false; }

        // Set card zone
        store.commit('cards/moveCard', { card, zone })

        // Put on Top of the Card Stack or bottom
        if (onBottom) {
            targetZone.prepend(cardElement);
        } else {
            targetZone.append(cardElement);
        }
    }

    handleTokenMove(tokenHtml, targetHtml, zone, store) {
        if (zone != Zones.Playfield) {
            store.dispatch('cards/removeToken', tokenHtml);
        } else {
            targetHtml.append(tokenHtml);
        }
    }

    getCardHtmlFromPileByIndex(pileType, cardIndex) {
        let validTypes = Object.values(Zones);
        if (!validTypes.includes(pileType)) { console.error(`Searching for card '${cardIndex}' in an unknown pileType: '${pileType}'`); return; }

        let pileHtml = document.querySelector(`.card-container[type="${pileType}"]`);
        if (!pileHtml) { console.error(`Could not find HTML for piletype: ${pileType}`); return; }

        let cardHtml = pileHtml.querySelector(`.card-wrapper[card-index="${cardIndex}"]`);
        if (!cardHtml) { console.error(`Could not find HTML for card: ${cardIndex}`); return; }

        return cardHtml;
    }

    getPileTypeFromElement(element) {
        let validSources = Object.values(Zones);

        let type = element.getAttribute('type');

        return validSources.includes(type) ? type : null;
    }

    async getCardFromSourceByElement(element, store) {

        let cardIndex = element.getAttribute('card-index');
        if (!cardIndex) {
            alert('card with unknown index!'); return;
        }

        return store.dispatch(`cards/getCardByIndex`, cardIndex);
    }

    shuffleDeck() {
        let deckContainer = document.querySelector(".deck.card-stack");
        this.shuffle(deckContainer, true);

        setTimeout(() => {
            if (!deckContainer.classList.contains("shuffling")) {
                deckContainer.classList.add("shuffling");
                setTimeout(() => {
                    deckContainer.classList.remove("shuffling");
                }, 2000);
            }
        })
    }

    async loadImage(path) {
        var img = document.createElement("img");
        img.src = path;

        return new Promise((resolve) => {
            img.onload = () => {
                resolve(img);
            }
        })
    }

    async processImageToDataUrl(file, resizeWidth, resizeHeight, border) {

        var imgCanvas = document.createElement("canvas"),
            imgContext = imgCanvas.getContext("2d");

        let url = URL.createObjectURL(file);
        let baseImage = await this.loadImage(url);

        let imageRatio = baseImage.width / baseImage.height;
        let resizeRatio = imageRatio;

        if (resizeWidth && resizeHeight) {
            resizeRatio = resizeWidth / resizeHeight;
        }

        let drawOffsetX = 0;
        let drawOffsetY = 0;

        // if Resize Ratio > imageRatio == result image would be wider -- else taller
        if (resizeRatio > imageRatio) {

            let sx = baseImage.width / resizeWidth;
            let y = baseImage.height / sx;
            let diff = y - resizeHeight;

            drawOffsetY = diff / 2;

        } else if (resizeRatio < imageRatio) {

            let sy = baseImage.height / resizeHeight;
            let x = baseImage.width / sy;
            let diff = x - resizeWidth;

            drawOffsetX = diff / 2;
        }

        imgCanvas.width = resizeWidth || baseImage.width;
        imgCanvas.height = resizeHeight || imgCanvas.width / imageRatio;

        // Draw image into canvas element

        if (border) {
            imgContext.drawImage(baseImage, (-drawOffsetX + 10), (-drawOffsetY + 10), imgCanvas.width + 2 * drawOffsetX - 20, imgCanvas.height + 2 * drawOffsetY - 20);
            let borderImage = await this.loadImage(require('@/assets/sleeve-border.png'));
            imgContext.drawImage(borderImage, 0, 0, imgCanvas.width, imgCanvas.height);
        } else {
            imgContext.drawImage(baseImage, -drawOffsetX, -drawOffsetY, imgCanvas.width + 2 * drawOffsetX, imgCanvas.height + 2 * drawOffsetY);
        }

        // Get canvas contents as a data URL
        return imgCanvas.toDataURL("image/jpeg", 0.7);
    }

    async saveImageFile(file, name, resizeWidth, resizeHeight, border) {

        if (!file) { console.log('No file was provided to save to LocalStorage'); return; }
        if (!name) { console.log('No name was given for the file to save in LocalStorage'); return; }

        let resized = await this.processImageToDataUrl(file, resizeWidth, resizeHeight, border);

        // Save image into localStorage
        try {
            localStorage.setItem(name, JSON.stringify({ name: file.name, data: resized }));
            return true;
        }
        catch (e) {
            console.log("Storage failed: " + e);
            return false;
        }
    }

    getStoredData(name) {
        try {
            let image = JSON.parse(localStorage.getItem(name));
            return image;

        } catch (e) {
            console.log(e);
            return;
        }
    }

    async getImageBlobUrl(imageData) {
        // Data url to Blob
        let response = await fetch(imageData);
        let blob = await response.blob();
        return URL.createObjectURL(blob);
    }

    /**
     * Store Deck in local storage
     * 
     * @param {object} deck Deck object to store
     * @param {string} deck.name Name of the Deck
     * @param {object} deck.url Url of the Deck
     * @param {object} [deck.theme] Optional Sleeve/Playmat references
     */
    saveDeck(deck) {

        // Remove deck if it already exist in list
        this.removeDeck(deck);

        // Get stored Decks
        let storedDecks = this.getStoredDecks();

        // push new deck
        storedDecks.push(deck);

        // Store
        localStorage.setItem("decks", JSON.stringify(storedDecks));
    }

    removeDeck(deck) {
        // Get stored Decks
        let storedDecks = this.getStoredDecks();

        // Check if there is any matching name & url in storage
        if (storedDecks.some(d => d.name == deck.name && d.url == deck.url)) {
            // replace existing deck in local storage
            let matches = storedDecks.filter(d => d.name == deck.name && d.url == deck.url);
            let index = storedDecks.indexOf(matches[0]);

            // Remove old reference in list
            storedDecks.splice(index, 1);
        }

        // Store
        localStorage.setItem("decks", JSON.stringify(storedDecks));
    }

    getStoredDecks() {
        return this.getStoredData("decks") || [];
    }

    async loadDeckTheme(deck, store) {
        if (!deck.theme) { console.log(`Deck didn't have a Theme to be loaded: ${JSON.stringify(deck)}`) }

        // Load background Image
        if (deck.theme.background) {
            let playmatUrl = await this.getImageBlobUrl(deck.theme.background.data);
            store.commit("setPlaymatUrl", playmatUrl);
        }

        // Load Sleeve Image
        if (deck.theme.sleeve) {
            let sleeveUrl = await this.getImageBlobUrl(deck.theme.sleeve.data);
            store.commit("cards/setCardSleeveUrl", sleeveUrl);
        }
    }
}

export default new Helper();