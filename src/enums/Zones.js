export default {
    Deck: 'deck',
    DeckTop: 'top',
    DeckBot: 'bot',
    Hand: 'hand',
    Playfield: 'playfield',
    Graveyard: 'graveyard',
    Exile: 'exile',
    CommandZone: 'commandzone',
    DeckPreview: 'deckpreview' // Temporary zone for Look At/Scry/Surveil Library
}