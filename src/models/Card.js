class Card {

    // Unique number assigned assigned to card by deck
    index;

    // ScryFall Data Object
    data = null;
    
    // Card States
    isVisible = false;
    isTapped = false;
    isRotated = false;
    isFlipped = false;
    isMelded = false;

    // Special Type of card booleans
    isSplitCard = false;
    isFlippableCard = false;
    isTurnableCard = false;
    isMeldableCard = false;

    // Keep Track of Zone in Card (?)
    isInZone = null;

    basePower = null;
    baseToughness = null;

    // Counters (any)
    counters = {
        "" : 0,
        "-1/-1": 0,
        "+1/+1": 0
    };
    
    constructor(json) {
        this.data = json;

        // Check for special cards
        let cd = this.data.card;
        this.isSplitCard = cd.card_faces.length == 2 && cd.image_uris && cd.mana_cost.includes("//");
        this.isTurnableCard = cd.card_faces.length == 2 && cd.image_uris && !cd.mana_cost.includes("//");
        this.isFlippableCard = cd.card_faces.length == 2 && !cd.image_uris;
        this.isMeldableCard = cd.keywords.includes("Meld");

        this.basePower = cd.power;
        this.baseToughness = cd.toughness;
    }

    get cardName() {
        if(!this.isVisible) { return 'Facedown Card' }
            
        if(this.isTurnableCard) {
            return this.isRotated ? this.data.card.card_faces[1].name : this.data.card.card_faces[0].name;
        }

        if(this.isFlippableCard) {
            return this.isFlipped ? this.data.card.card_faces[1].name : this.data.card.card_faces[0].name;
        }

        if(this.isMeldableCard && this.isMelded) {
            return this.data.melded.name;
        }

        return this.data.card.name;
    }
}

export default Card;