import Vue from 'vue'
import App from './App.vue'
import store from './store/index';

import vuetify from './plugins/vuetify';
import dragula from './plugins/dragula';

// Import Dragula
import 'dragula/dist/dragula.css'

Vue.use(dragula, {
  direction: 'horizontal',
  isContainer: (el) => { return el.classList.contains('card-container') || el.classList.contains('card-drop') },
  accepts: (el, target) => { return target.classList.contains('card-drop') },
  slideFactorX: 10,
  slideFactorY: 10,
});

Vue.config.productionTip = false;

new Vue({
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app') 
