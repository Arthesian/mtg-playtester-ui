import Dragula from 'dragula';

class DragulaPlugin {
    install(Vue, options) {
        Vue.prototype.$drake = new Dragula(options);
    }
}

export default new DragulaPlugin();